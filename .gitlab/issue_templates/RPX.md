![application screenshot](https://www.wiiubru.com/appstore/packages/something/screen1.png)
https://apps.fortheusers.org/wiiu/something

# What's the app?
(very quick description of what the app is and does)

# Does it work?
(does the app actually work?)

# Direct port blockers
- :white_check_mark:/:x: doesn't stay resident in memory during a title switch
- :white_check_mark:/:x: doesn't chainload other homebrew
- :white_check_mark:/:x: doesn't call kern_read or kern_write
- :white_check_mark:/:x: doesn't access HBL memory area outside of normal dynamic_libs/OSSpecifics stuff
- :white_check_mark:/:x: uses normal HBL folder layout (common, dynamic_libs, fs, etc.)

# Porting notes
(any notes around porting, like whether a version exists, a Switch backport, an unusual folder layout, etc.)

/label ~untriaged ~"wut+rpx"