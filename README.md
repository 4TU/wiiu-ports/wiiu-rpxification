# wiiu-rpxification

Project tracker to modernise wiiu apps, or send them to the depths of the legacy section

Please see the [boards](https://gitlab.com/4TU/wiiu-ports/wiiu-rpxification/-/boards)

## Why?
Upcoming changes to the homebrew environment will make legacy .elf homebrew difficult to load, and a proper ProcUI loop will be essential for wut homebrew. This project tracks our efforts to make a library of homebrew ready in time for these changes.

Changed code should either be merged back into the upstream repo (if the dev is still active) or put in a public repo under 4TU/wiiu-ports. If you don't have access to make a repository and would like to take on an app, please comment in its issue and we'll get you hooked up!